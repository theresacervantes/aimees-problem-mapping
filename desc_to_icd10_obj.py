import pandas as pd
import numpy as np
import re, requests
import multiprocessing as mp
from collections import Counter
from bs4 import BeautifulSoup
from time import sleep
from unidecode import unidecode
from sklearn.feature_extraction.text import TfidfVectorizer
from random import randint
from fake_useragent import UserAgent
from sklearn.metrics.pairwise import cosine_similarity
from ast import literal_eval
from timeit import default_timer as timer
    

class desc_to_icd10_obj:
    """Converts free-text descriptions in problem records to icd10 codes using cosine similarity on a corpus of existing
    mappings in the tier-1 database. Provides cosine-simiarlity based confidence scores for each mapping"""
    
    featMax = 7500;
    ua = UserAgent()

    #def __init__(self, name):
   #     self.name = name


    #Old corpus from the greenplum database below - don't delete until we figure out how well the new one works
    #df_greenplum_mapped_icd10_1 = pd.read_csv('GreenplumKensciSunriseSalfordGoogleMappings_mapped_to_hcc.txt',sep='\t').dropna()
    #df_greenplum_mapped_icd10_2 = pd.read_csv('GreenplumKensciSunriseSalfordGoogleMappings_unmapped_to_hcc.csv').dropna()\
    #    [['name', 'icd10', 'real desc of icd10']]
    #df_greenplum_mapped_icd10_2.rename(index=str, columns={'real desc of icd10':'desc'}, inplace=True)
    #df_greenplum_mapped_icd10_3 = pd.read_csv('GreenplumKensciSunriseSalfordGoogleMappings.txt',sep='|').dropna()
    
    officialIcd10 = pd.read_csv('icd10cm_codes_2019.csv')
    officialIcd10['icd10'] = officialIcd10.icd10.apply(lambda t: t[0:3] + '.' + t[3:])
    officialIcd10['name'] = officialIcd10.description
    officialIcd10.rename(columns={'description':'desc'}, inplace=True)
    officialIcd10 = officialIcd10[['name', 'icd10']]
    corp = pd.read_csv('newCorpus.csv')
    
    dfg = pd.concat([corp, officialIcd10], ignore_index=True)
    dfg['name'] = dfg.name.apply(str.lower)

    #del df_greenplum_mapped_icd10_1
    #del df_greenplum_mapped_icd10_2
    #del df_greenplum_mapped_icd10_3
    #del officialIcd10

    #set up icd9-icd10 mapping
    df_mappings = pd.read_csv('icd9_and_icd10',sep='|')
    icd10_cms = pd.read_csv('icd10cm_codes_2019.csv')
    icd10_cms.icd10 = icd10_cms.icd10.apply(lambda icd: icd[:3] + '.' + icd[3:])
    icd10_cms.set_index('icd10', inplace=True)
    
    #import parent archive
    parentDf = pd.read_csv('parentDf.csv', converters={"lstOfParents": literal_eval})
    parentDf.set_index('desc', inplace=True) #pandas indexing uses hashing

    def bing_search_icd10(self, desc0):
        """Searches Bing to find a list of possible ICD-10 parents - the first three characters of an ICD-10 code - 
        for a free-text description. Using Bing because Google will block you if you send too many requests."""
        results = self.bing_search_icd10_results(desc0)
        if len(results) == 0:
            sleep(3)
            results = self.bing_search_icd10_results(desc0)
        try:
            if results[0][1] > 2: 
                return [i[0] for i in results]
            else:
                return ''
        except IndexError:
            return ''

    def bing_search_icd10_results(self, desc0):
        """Helper function for bing_search_icd10"""
        desc = '+'.join(desc0.split(' '))
        url = 'https://www4.bing.com/search?q={}+icd10'.format(desc)
        r = requests.get(url, headers={'User-Agent': self.ua.random})
        soup = BeautifulSoup(r.content,'html.parser')
        soup_text = ' '.join(soup.findAll(text=True))
        possible_parents = re.findall(r'\b[A-Z][0-9]{2}', soup_text)
        found_icd10s = Counter(possible_parents)
        return found_icd10s.most_common(2)


    def icd10Desc(self, code):
        """Gets the description associated with an ICD-10 code"""
        try:
            desc = self.icd10_cms.description.loc[code]
        except KeyError:
            url = 'https://icdcodelookup.com/icd-10/codes/{}'.format(code)
            r = requests.get(url, headers={'User-Agent': self.ua.random})
            soup = BeautifulSoup(r.content,'html.parser')
            try:
                desc = soup.findAll('div', class_='topWrapper')[0].h3.span.text;
            except:
                return None
            #update the dataframe so we don't have to search the same thing a million times
            self.icd10_cms.loc[code] = [desc];
        return desc

    def find_match_cs(self, desc, repFlag=False):
        """Uses cosine similarity to find the closest matching description in the corpus to a new description. Then
        reports the ICD-10 code associated with the matching description and a confidence score based on cosine similarity"""
        #start = timer()
        desc = desc.lower()
        try:
            lst_of_parents = self.parentDf.lstOfParents.loc[desc]
        except KeyError:
            lst_of_parents = self.bing_search_icd10(desc)
            if lst_of_parents != '':
                self.parentDf = self.parentDf.append(pd.Series({'lstOfParents': lst_of_parents}, name=desc))
            else:
                return (desc, '', '', np.NaN)
            print('Searched Bing')
        df_codes = self.dfg[self.dfg.icd10.str.contains('|'.join(lst_of_parents))].reset_index(drop=True)
        X0 = df_codes.name
        tfidf = TfidfVectorizer(analyzer='word', stop_words='english')
        try:
            X = tfidf.fit_transform(X0)#.toarray()
        except ValueError:
            try: 
                tfidf2 = TfidfVectorizer(analyzer='word', stop_words=None)
                X = tfidf2.fit_transform(X0)#.toarray()
            except ValueError:
                return (None, None, None, 0)
        new_desc1 = tfidf.transform([desc])#.toarray()
        cs = cosine_similarity(X, new_desc1)
        ix = cs.argmax()    
        icd10 = df_codes.icd10[ix]
        score = cs[ix,0]
        name = self.icd10Desc(icd10) #official description
        #end = timer()
        #elapsed = end - start
        #with open('total_timings.txt', 'a') as f:
        #    f.write(str(elapsed) + '\n')
        if not repFlag and (icd10 == '' or desc == None): #sometimes the tool randomly doesn't report results
            (desc, icd10, name, score) = find_match_cs2(self, desc, True)
        return (desc, icd10, name, score)

    def getParentDf(self):
        """Getter for the dataframe with lists of parents for previously searched descriptions"""
        return self.parentDf
    
    def saveSearches(self):
        """Updates parentDf with bing searches we had to perform for new descriptions"""
        self.parentDf.to_csv('parentDf.csv')
        
    def confidence(self, icd10_mapped, desc):
        """Compares the description associated with a mapped ICD-10 code to all descriptions mapped to that code in our corpus
        and reports the maximum cosine similarity as a confidence score"""
        if icd10_mapped == None or desc == '' or desc == None:
            return None
        lst_of_parents = [icd10_mapped.split('.')[0]]
        df_codes = self.dfg[self.dfg.icd10.str.contains('|'.join(lst_of_parents))].reset_index(drop=True)
        X0 = df_codes.name
        #print(len(X0))
        tfidf = TfidfVectorizer(analyzer='word', stop_words='english')
        try:
            X = tfidf.fit_transform(X0)#.toarray()
        except ValueError:
            try: 
                tfidf2 = TfidfVectorizer(analyzer='word', stop_words=None)
                X = tfidf2.fit_transform(X0)#.toarray()
            except ValueError:
                return None
        new_desc1 = tfidf.transform([desc])#.toarray()
        cs = cosine_similarity(X, new_desc1)
        ix = cs.argmax()    
        icd10 = df_codes.icd10[ix]
        score = cs[ix,0]
        return score
    
    def standardize(self, name):
        """Helper method for pre-processing descriptions"""
        out = name.lower()
        for p in ("-", "_", "~", ':', ';', '|', '+'):
            out = out.replace(p, ' ')
        for x in ('.', ',', '?', "'", '"', '`', '!', '*', '(', ')', '#'):
            out = out.replace(x, '')
        #replace multiple occurrences of 'z' (what the heck)
        out = re.sub('zz+', '', out)
        out = re.sub('  +', ' ', out)
        return out.strip()
    
    def jaccardSimilarity(self, desc1, desc2):
        """Alternate similarity score, not in use"""
        #computes the jaccard similarity between two descriptions
        #senstive to spelling errors, unfortunately
        desc1 = self.standardize(desc1)
        desc2 = self.standardize(desc2)
        set1 = set(desc1.split())
        set2 = set(desc2.split())
        if len(set1) == 0 and len(set2) == 0:
            return 1
        score = len(set1 & set2)/len(set1 | set2)
        return score

    def overlapCoeff(self, desc1, desc2):
        """Alternate similarity score, not in use"""
        desc1 = self.standardize(desc1)
        desc2 = self.standardize(desc2)
        set1 = set(desc1.split())
        set2 = set(desc2.split())
        if len(set1) == 0 or len(set2) == 0:
            return 0
        return len(set1 & set2)/min(len(set1), len(set2))



